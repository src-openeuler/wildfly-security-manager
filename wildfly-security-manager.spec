%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}

Name:                wildfly-security-manager
Version:             1.1.2
Release:             3
Summary:             WildFly Security Manager
License:             ASL 2.0 and LGPLv2+
Url:                 https://github.com/wildfly-security/security-manager/
Source0:             https://github.com/wildfly-security/security-manager/archive/%{namedversion}/wildfly-security-manager-%{namedversion}.tar.gz

Patch0:              0001-Remove-reliance-on-derecated-methods-and-sun-specific-APIs.patch
BuildRequires:       maven-local mvn(junit:junit) mvn(org.jboss:jboss-parent:pom:)
BuildRequires:       mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-annotations)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:       mvn(org.jboss.logmanager:jboss-logmanager) mvn(org.jboss.modules:jboss-modules)
BuildRequires:       mvn(org.kohsuke.metainf-services:metainf-services)
BuildRequires:       mvn(org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools
BuildArch:           noarch

%description
The Security Manager for WildFly Application Server.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n security-manager-%{namedversion}
%patch0 -p1
%pom_remove_plugin "org.jboss.seven2six:seven2six"
%pom_add_dep org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:1.0.2.Final
sed -i 's/@Test//g' src/test/java/org/wildfly/security/manager/TestStackInspector.java

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build

%install
%mvn_install

%files -f .mfiles

%files javadoc -f .mfiles-javadoc

%changelog
* Tue Aug 01 2023 Ge Wang <wang__ge@126.com> - 1.1.2-3
- Compile with jdk11 due to jboss-modules updated

* Fri Dec 24 2021 wangkai <wangkai385@huawei.com> - 1.1.2-2
- This package depends on log4j.After the log4j vulnerability CVE-2021-45105 is fixed,the version needs to be rebuild.

* Mon Aug 17 2020 maminjie <maminjie1@huawei.com> - 1.1.2-1
- package init
